import React from "react";
import top1 from "../../../assets/svg/top1.svg";
import top2 from "../../../assets/svg/top2.svg";
import top3 from "../../../assets/svg/top3.svg";
import top4 from "../../../assets/svg/top4.svg";

import "../../../style/FeatureCard/card.css";

const FeatureList2 = () => {
  return (
    <div
      className="flex text-center justify-self-center items-center 
    justify-center -translate-y-[300px] lg:-translate-y-[250px] xl:-translate-y-[200px] 
    2xl:-translate-y-[350px] "
    >
      <div
        className="flex flex-col bg-[#EDE2D6] border-2 
      border-dashed border-[#4A4743] w-4/5 lg:w-11/12 xl:11/12 
      font-[Inter] xl:h-[480px] 2xl:h-[500px]"
      >
        <div data-aos="zoom-in" className="my-3">
          <p
            className="text-[#993333] font-bold text-xl
          md:text-2xl lg:text-4xl xl:text-5xl 2xl:text-5xl font-[Inter] sm:font-extrabold"
          >
            TOP FEATURES
          </p>
        </div>
        <div
          className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 
          xl:grid-cols-4 lg:mb-3 2xl:grid-cols-4 items-center 
          justify-center gap-2 md:mb-5"
        >
          <div
          data-aos="flip-left"
            className="items-center justify-center bg-[#EBEBEB] 
          justify-self-center mx-2 rounded-md py-2 md:h-[350px] lg:h-[360px] xl:h-[350px] lg:py-2 2xl:h-[400px]"
          >
            <div
              className=" flex w-full items-center justify-center place-items-center mx-auto 
            justify-self-center justify-items-center"
            >
              <img src={top1} alt="top1" className="px-2 rounded-md" />
            </div>
            <div className="px-3 lg:px-1 2xl:px-8">
              <p className="text-[#8B3E39] font-bold text-sm md:text-lg lg:text-xl xl:text-xl 2xl:text-2xl">
                FlokiRush Game
              </p>
              <p className="text-[#707070] text-xs md:text-sm  lg:text-base xl:text-base 2xl:text-lg">
                Experience fun and simple minigame concept and unlimited
                possibilities for lifetime earnings.
                <br />
              </p>
            </div>
          </div>
          <div data-aos="flip-left" className="bg-[#EBEBEB] mx-2 rounded-md py-2 md:h-[350px] lg:h-[360px] xl:h-[350px] 2xl:h-[400px]">
            <div className=" flex w-full items-center justify-center place-items-center mx-auto justify-self-center justify-items-center">
              <img src={top2} alt="top2" className="px-2 rounded-md" />
            </div>
            <div className="px-3 lg:px-1 2xl:px-8">
              <p className="text-[#8B3E39] font-bold text-sm md:text-lg lg:text-xl xl:text-xl 2xl:text-2xl">
                Doge Bank
              </p>
              <p className="text-[#707070] text-xs md:text-sm lg:text-base xl:text-base 2xl:text-lg">
                Allocation goes into the DOGE Vault, about 10% from total
                transaction and 10% between wallets
              </p>
            </div>
          </div>
          <div data-aos="flip-left" className="bg-[#EBEBEB] mx-2 rounded-md py-2 md:h-[350px] lg:h-[360px] xl:h-[350px] 2xl:h-[400px]">
            <div className=" flex w-full items-center justify-center place-items-center mx-auto justify-self-center justify-items-center">
              <img src={top3} alt="top3" className="px-2 rounded-md" />
            </div>
            <div className="px-3 lg:px-1 2xl:px-8">
              <p className="text-[#8B3E39] font-bold text-sm md:text-lg lg:text-xl xl:text-xl 2xl:text-2xl">
                Top 50 Club
              </p>
              <p className="text-[#707070] text-xs md:text-sm lg:text-base xl:text-base 2xl:text-lg">
                Top 50 Club Reward Hodl and DCA your way to be a Top 50 club and
                claim daily rewards incentivized specifically for them.
              </p>
            </div>
          </div>
          <div data-aos="flip-left" className="bg-[#EBEBEB] mx-2 rounded-md py-2 md:h-[350px] lg:h-[360px] xl:h-[350px] 2xl:h-[400px]">
            <div className=" flex w-full items-center justify-center place-items-center mx-auto justify-self-center justify-items-center">
              <img src={top4} alt="top4" className="px-2 rounded-md" />
            </div>
            <div className="px-3 lg:px-1 2xl:px-8">
              <p className="text-[#8B3E39] font-bold text-sm md:text-lg lg:text-xl xl:text-xl 2xl:text-2xl">
                Upcoming NFTs
              </p>
              <p className="text-[#707070] text-xs md:text-sm lg:text-base xl:text-base 2xl:text-lg">
                Experience fun and simple minigame concept and unlimited
                possibilities for lifetime earnings.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FeatureList2;
