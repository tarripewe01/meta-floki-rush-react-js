import React from "react";
import token1 from "../../../assets/image/token1.png";
import token2 from "../../../assets/image/token2.png";
import token3 from "../../../assets/image/token3.png";
import token4 from "../../../assets/image/token4.png";

const TokenList = () => {
  return (
    <div
      className="grid grid-cols-4 sm:grid-cols-1 mx-auto gap-20 lg:gap-10 
    md:gap-0 sm:gap-0 font-[Inter] "
    >
      <div className=" w-[200px] h-[450px] sm:h-[150px]  ">
        <div className="">
          <img
            src={token1}
            alt="token1"
            className="mx-auto h-[200px] w-[200px] lg:h-[150px] lg:w-[150px]
            md:h-[90px] md:w-[90px] sm:h-[70px] sm:w-[70px]"
          />
          <p className="font-bold text-2xl md:text-lg sm:text-base 2xl:text-xl lg:text-xl text-[#3AB682]">
            Total Supply
          </p>
          <p className="text-lg mt-3 md:text-sm sm:text-xs 2xl:text-base lg:text-base ">
            1,000,000,000,000
          </p>
        </div>
      </div>
      <div className=" w-[200px] sm:h-[250px] ">
        <div className="">
          <img
            src={token3}
            alt="token3"
            className="mx-auto h-[210px] w-[200px] -mt-3 
            lg:h-[150px] lg:w-[150px]
            md:h-[90px] md:w-[90px] sm:h-[70px] sm:w-[70px]"
          />

          <p className="font-bold text-2xl md:text-lg sm:text-base 2xl:text-xl lg:text-xl text-[#E8646D]">
            Rewards
          </p>
          <p className="text-lg mt-3 md:text-sm sm:text-xs 2xl:text-base  lg:text-base">
            5-7% Doge-Bank as $DOGE rewards
          </p>
          <p className="text-lg mt-20 md:text-sm sm:text-xs sm:mt-[10px]  2xl:text-base lg:text-base">
            1-2% TOP 50 Club rewards
          </p>
        </div>
      </div>
      <div className=" w-[200px] h-[450px] sm:h-[150px] sm:-mt-[70px] ">
        <div className="">
          <img
            src={token2}
            alt="token2"
            className="mx-auto h-[200px] w-[200px] lg:h-[150px] lg:w-[150px]
            md:h-[90px] md:w-[90px] sm:h-[70px] sm:w-[70px]"
          />
          <p className="font-bold text-2xl md:text-lg sm:text-base 2xl:text-xl lg:text-xl text-[#F2A458]">
            Game Development
          </p>
          <p className="text-lg mt-3 md:text-xs sm:text-xs 2xl:text-base  lg:text-base">
            2% NFT & P2E Games Development
          </p>
        </div>
      </div>
      <div className=" w-[200px] sm:h-[150px]">
        <div className="">
          <img
            src={token4}
            alt="token4"
            className="mx-auto h-[200px] w-[200px] lg:h-[150px] lg:w-[150px]
            md:h-[90px] md:w-[90px] sm:h-[70px] sm:w-[70px]"
          />
          <p className="font-bold text-2xl md:text-lg sm:text-base text-[#2D9AEF] 2xl:text-xl lg:text-xl">
            Others
          </p>
          <p className="text-lg mt-3 md:text-sm sm:text-xs 2xl:text-base lg:text-base ">
            3-4% Marketing
          </p>
          <p className="text-lg mt-20 md:text-sm sm:text-xs sm:mt-[10px] 2xl:text-base lg:text-base ">
            1% Liquidity Pool
          </p>
        </div>
      </div>
    </div>
  );
};

export default TokenList;
