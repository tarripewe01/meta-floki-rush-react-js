import React from "react";
import flokiRoadmap from "../../../assets/image/flokiRoadmap.png";

const Info2 = () => {
  return (
    <div
      id="TopFeatures"
      className="grid grid-cols-2 sm:grid-cols-1 mx-auto px-10 lg:px-12 
      2xl:-translate-y-[350px]
      xl:-translate-y-[200px] lg:-translate-y-[250px] md:-translate-y-[300px]
      sm:-translate-y-[340px] "
    >
      <div className=" justify-center items-center" data-aos="fade-right">
        <div
          className="bg-[#EBEBEB] mx-auto border-[#91BFDE] border-[15px] sm:border-[8px]
            rounded-lg shadow-md lg:py-6 xl:py-10
            2xl:py-5 2xl:w-[500px] justify-end items-end 2xl:mt-24
            2xl:ml-72"
        >
          <div
            className="text-center font-[Inter] py-3 
          2xl:px-[30px] justify-end items-end 2xl:py-2 "
          >
            <p
              className="text-[#4680C2] text-sm lg:text-base xl:text-3xl
          2xl:text-3xl font-bold mb-4"
            >
              Experience simple, fun, and nostalgic game concept
            </p>
            <p
              className="text-[#8E8E8E] text-xs lg:text-md xl:text-xl
          2xl:text-lg"
            >
              Our endless-runner game is the top feature in Meta FlokiRush
              Token, bringing endless progress in both gameplay and earning
              possibility
            </p>
          </div>
        </div>
      </div>
      <div className="2xl:-ml-[100px]">
        <img
          src={flokiRoadmap}
          alt="flokiRoadmap"
          className="w-[320px] sm:w-[300px] lg:w-[400px] xl:w-[550px] 
          2xl:w-[500px] 2xl:-mt-20 mx-auto xl:-mt-[120px] lg:-mt-[100px]
          md:-mt-[100px]"
          data-aos="fade-left"
        />
      </div>
    </div>
  );
};

export default Info2;
