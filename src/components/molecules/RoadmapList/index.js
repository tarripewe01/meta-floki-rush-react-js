import React from "react";
import "../../../style/RoadmapList/style.css";

const RoadmapList = () => {
  return (
    <div
      className="grid grid-cols-1 mx-auto justify-center items-center gap-2 md:grid-cols-2
    lg:grid-cols-2 lg:-ml-24 xl:grid-cols-2 xl:-ml-28 2xl:grid-cols-2 
    2xl:-ml-44 md:translate-y-7"
    >
      <div class="card">
        <div class="front1">
          <div class="front-content">
            <div>
              <div className="bg-white w-20 mt-2 ml-3  rounded-md text-center xl:w-24 2xl:w-32">
                <p className="text-[#922626] text-[8px] font-semibold xl:text-md 2xl:text-lg">
                  Phase 1
                </p>
              </div>
              <p
                className="font-bold text-xs text-center md:text-base xl:text-xl xl:mt-4 
              2xl:text-xl 2xl:mt-6"
              >
                PREPARATION !!
              </p>
              <p className="text-[10px] px-10 mt-2 md:text-sm text-center xl:text-base 2xl:text-lg 2xl:mt-4">
                This is the early stages of the project, we try to prepare
                things up before set to presale
              </p>
            </div>
          </div>
        </div>
        <div class="back1">
          <div class="back-content1 middle1">
            <div className="text-center text-[8px] md:text-sm xl:text-base 2xl:text-base">
              <p>
                1. Initiation
                <br /> (Website, Smart Contract Creation)
                <br /> 2. Prelaunch Marketing
                <br />
                3. Presale & DEX Listing
                <br />
                4. CG & CMC Listing
                <br />
                5. Project Audit
                <br />
                6. NFT & Game Preview
                <br />
                7. 1500 Holders
                <br />
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="front2">
          <div class="front-content">
            <div>
              <div className="bg-white w-20 mt-2 ml-3 rounded-md text-center xl:w-24 2xl:w-32">
                <p className="text-[#922626] text-[8px] font-semibold xl:text-md 2xl:text-lg">
                  Phase 2
                </p>
              </div>
              <p className="font-bold text-xs text-center md:text-base xl:text-xl xl:mt-4 2xl:text-xl 2xl:mt-6">
                READY !?
              </p>
              <p className="text-[10px] px-10 md:text-sm text-center xl:text-base 2xl:text-lg 2xl:mt-4">
                From public recognition to ample marketing push for the project,
                gearing up for a huge mini-game launch.
              </p>
            </div>
          </div>
        </div>
        <div class="back1">
          <div class="back-content1 middle1">
            <div className="text-center text-[10px] md:text-sm xl:text-base 2xl:text-base">
              <p>
                1. AMA’s
                <br />
                2. Marketing push to spread awareness
                <br />
                3. P2E Game Beta Run
                <br />
                4. Huge Marketing Campaign 10.000 Holders
                <br />
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="front3">
          <div class="front-content">
            <div>
              <div className="bg-white w-20 mt-2 ml-3 rounded-md text-center xl:w-24 2xl:w-32">
                <p className="text-[#922626] text-[8px] font-semibold xl:text-md 2xl:text-lg">
                  Phase 3
                </p>
              </div>
              <p className="font-bold text-xs text-center md:text-base xl:text-xl xl:mt-4 2xl:text-xl 2xl:mt-6">
                GET SET !!
              </p>
              <p className="text-[10px] px-10 md:text-sm text-center xl:text-base 2xl:text-lg 2xl:mt-4">
                Post-P2E Minigame release will be followed by our very own
                marketplace and Huge CEX that'll follow.
              </p>
            </div>
          </div>
        </div>
        <div class="back1">
          <div class="back-content1 middle1">
            <div className="text-center text-[10px] md:text-sm xl:text-base 2xl:text-base">
              <p>
                1. NFT marketplace Beta Release
                <br /> 2. Strategic Marketing Campaign
                <br />
                3. P2E Game Official Launch
                <br />
                4. Certix Audit
                <br />
                5. CEX Losting
                <br />
                6. 25.000 Holders
                <br />
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="front4">
          <div class="front-content">
            <div>
              <div
                className="bg-white w-20 mt-2 ml-3 rounded-md text-center xl:w-24 
              2xl:w-32"
              >
                <p
                  className="text-[#922626] text-[8px] font-semibold xl:text-md 
                2xl:text-lg"
                >
                  Phase 4
                </p>
              </div>
              <p
                className="font-bold text-xs text-center text-white md:text-base 
              xl:text-xl xl:mt-4 2xl:text-xl 2xl:mt-3"
              >
                GOO!!!
              </p>
              <p
                className="text-[10px] px-2 mt-0 md:text-sm text-center text-white 
              xl:text-base 2xl:text-lg 2xl:mt-2"
              >
                Going into the METAVERSE is always the end-game, and the
                MetaFlokiVerse is an exciting era for the Flokis to rule. New
                metaverse concept for the MetaFlokiRush holder's to experience.
              </p>
            </div>
          </div>
        </div>
        <div class="back1">
          <div class="back-content1 middle1">
            <div className="text-center text-[10px] md:text-sm xl:text-base 2xl:text-base">
              <p>
                1. NFT marketplace Beta Release
                <br /> 2. Strategic Marketing Campaign
                <br />
                3. P2E Game Official Launch
                <br />
                4. Certix Audit
                <br />
                5. CEX Losting
                <br />
                6. 25.000 Holders
                <br />
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RoadmapList;
