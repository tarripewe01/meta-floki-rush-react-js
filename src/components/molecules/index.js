import Navbar from "./Navbar";
import Info from "./Info";
import Support from "./Support";
import FunInfo from "./FunInfo";
import FeaturesList from "./FeaturesList";
import TokenList from "./TokenList";
import HowToBuyList from "./HowToBuyList";

export {
  Navbar,
  Info,
  Support,
  FunInfo,
  FeaturesList,
  TokenList,
  HowToBuyList,
};
