import React, { useState } from "react";
import { AiOutlineClose } from "react-icons/ai";
import { GiHamburgerMenu } from "react-icons/gi";
import instagram from "../../../assets/svg/instagram.svg";
import reddit from "../../../assets/svg/reddit.svg";
import telegram from "../../../assets/svg/telegram.svg";
import twitter from "../../../assets/svg/twitter.svg";

const NavbarMobile = () => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <div div className="flex items-center justify-center z-50">
      <div
        className="flex  md:hidden lg:hidden xl:hidden 2xl:hidden 3xl:hidden
        absolute top-0 w-full justify-between items-center bg-[#F7BD3F] px-5 py-3 "
      >
        {/* <div className="w-14">
          <img src={logo} alt="logo" />
        </div> */}
        <div className="flex md:hidden space-x-3">
          <a href="https://www.instagram.com/meta.flokirush/" target="_blank" rel="noreferrer">
            <img src={telegram} alt="telegram" className="sm:w-5" />
          </a>
          <a href="https://twitter.com/metaflokirush" target="_blank" rel="noreferrer">
            <img src={twitter} alt="twitter" className="sm:w-5" />
          </a>
          <a href="https://www.instagram.com/meta.flokirush/" target="_blank" rel="noreferrer">
            <img src={instagram} alt="instagram" className="sm:w-6" />
          </a>
          <a href="https://www.reddit.com/user/metaflokirush" target="_blank" rel="noreferrer">
            <img src={reddit} alt="reddit" className="sm:w-5" />
          </a>
        </div>
        <div onClick={() => setIsOpen(!isOpen)}>
          {isOpen ? (
            <AiOutlineClose className=" text-lg" />
          ) : (
            <GiHamburgerMenu className=" text-lg" />
          )}
        </div>
      </div>
      <div
        className={`sm:flex flex-col hidden w-full absolute  items-center justify-center bg-[#F7BD3F]
  font-[Inter] font-medium px-5 py-4 uppercase  ${isOpen ? "top-10" : "top-[-450px]"}`}
      >
        <a href="#Home" className="hover:text-[#7a2605] ">
          Home
        </a>
        <a href="#TopFeatures" className="hover:hover:text-[#7a2605]">
          Features
        </a>
        <a href="#Roadmap" className="hover:hover:text-[#7a2605]">
          Roadmap
        </a>
        <a className="hover:hover:text-[#7a2605]">Whitepaper</a>
        <a className="hover:hover:text-[#7a2605]">Minigame(coming soon)</a>
        <a className="hover:hover:text-[#7a2605]">Marketplace(coming soon)</a>
        <div
          className="bg-[#7a2605] w-3/4 mx-20 px-4 py-2 text-center text-white
        rounded-lg border-2 border-[#FFC84E] hover:border-[#7a2605] mt-3"
        >
          <button className="font-bold">Doge Bank</button>
        </div>
      </div>
    </div>
  );
};

export default NavbarMobile;
