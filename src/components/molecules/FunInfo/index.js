import React from "react";

import flokiFun from "../../../assets/image/flokiFun.png";

const FunInfo = () => {
  return (
    <div
      className="flex grid-cols-2 px-8 items-center -translate-y-[1280px]
    lg:-translate-y-[1150px] xl:-translate-y-[950px] 2xl:-translate-y-[800px]
    "
    >
      <div>
        <img
          src={flokiFun}
          alt="flokiFun"
          className="h-[250px] md:h-[300px] lg:h-[450px] xl:h-[550px] 2xl:h-[700px]"
        />
      </div>
      <div
        className="w-[300px] py-3 bg-[#EBEBEB] rounded-md border-8 border-[#5FA1D1] px-6  font-[Avro] text-center
      md:w-[350px] md:py-4 lg:w-[450px] xl:w-[600px] 2xl:w-[700px]
      "
      >
        <p
          className="text-[#4580C2] font-bold text-xl mb-3 md:text-2xl 
        lg:text-4xl xl:text-5xl 2xl:text-6xl "
        >
          Experience simple, fun, and nostalgic game concept.
        </p>
        <p className="text-[#383636] text-xs md:text-sm lg:text-xl xl:text-3xl xl:mt-10 2xl:text-4xl">
          Our endless-runner game is the top feature in FlokiRush Token,
          bringing endless progress in both gameplay and earning possibility.
        </p>
      </div>
    </div>
  );
};

export default FunInfo;
