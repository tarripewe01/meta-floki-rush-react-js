import React from "react";
import support1 from "../../../assets/image/support1.png";
import support2 from "../../../assets/image/support2.png";
import support3 from "../../../assets/image/support3.png";

const Support = () => {
  return (
    <div
      className="flex w-full justify-evenly items-center translate-y-[-360px] 
  lg:translate-y-[-330px] xl:translate-y-[-270px] 
  2xl:translate-y-[-380px] 2xl:px-[150px]  "
    >
      <img
        src={support1}
        alt="support1"
        className="xl:w-[250px] 2xl:w-[300px] sm:w-[80px]" data-aos="zoom-in"
      />
      <img
        src={support2}
        alt="support2"
        className="xl:w-[250px] 2xl:w-[300px] sm:w-[80px]" data-aos="zoom-in"
      />
      <img
        src={support3}
        alt="support3"
        className="xl:w-[80px] 2xl:w-[70px] sm:w-[20px]" data-aos="zoom-in"
      />
    </div>
  );
};

export default Support;
