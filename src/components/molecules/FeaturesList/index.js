import React from "react";

import top1 from "../../../assets/svg/top1.svg";
import top2 from "../../../assets/svg/top2.svg";
import top3 from "../../../assets/svg/top3.svg";
import top4 from "../../../assets/svg/top4.svg";

import "../../../style/FeatureCard/card.css";

const FeaturesList = () => {
  return (
    <div
      className="bg-[#EDE2D6] w-[650px] ml-5 items-center justify-center justify-self-center 
      place-self-center place-content-center border-dashed border-2 border-[#4A4743] py-4 
      -translate-y-[300px] sm:w-full sm:ml-0 md:w-3/4
      xl:w-[1050px] xl:-translate-y-[200px] 2xl:w-[1270px] 2xl:-translate-y-[100px]"
    >
      <div className="w-full text-center font-[Arvo] font-bold my-3">
        <p className="text-3xl text-[#993333] xl:text-4xl lg:text-5xl 2xl:text-6xl ">
          TOP FEATURES
        </p>
      </div>
      <div
        className="grid grid-cols-2 font-[Arvo] px-9 space-y-3 items-center 
        justify-center justify-self-center justify-items-center ml-6 pb-3 sm:grid-cols-1 lg:ml-0 
       lg:grid-cols-4 xl:grid-cols-4 xl:-ml-4 xl:gap-4 2xl:grid-cols-4 2xl:ml-2 sm:w-full 
        sm:px-0 sm:ml-0 sm:space-y-2 md:gap-3"
      >
        <div class="card">
          <div class="front">
            <img
              src={top1}
              alt="top1"
              className="object-cover sm:w-[200px] sm:h-[170px] lg:w-[170px] lg:h-[270px] 
              md:w-[270px] md:h-[230px] md:object-cover"
            />
          </div>
          <div class="back">
            <div class="back-content middle">
              <h2>FlokiRush Game</h2>
              <p>
                Experience fun and simple minigame concept and unlimited
                possibilities for lifetime earnings.
              </p>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="front">
            <img
              src={top2}
              alt="top2"
              className="object-cover sm:w-[200px] sm:h-[170px] lg:w-[170px] lg:h-[270px] 
              md:w-[270px] md:h-[230px] md:object-cover lg:object-cover"
            />
          </div>
          <div class="back">
            <div class="back-content middle">
              <h2>Doge Coinbank</h2>
              <p>
                Allocation goes into the DOGE Vault, about 10% from total
                transaction and 10% between wallets
              </p>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="front">
            <img
              src={top3}
              alt="top3"
              className="object-cover sm:w-[200px] sm:h-[170px] lg:w-[170px] lg:h-[270px] 
              md:w-[270px] md:h-[230px] md:object-cover lg:object-cover"
            />
          </div>
          <div class="back">
            <div class="back-content middle">
              <h2>Top 50 Club Reward</h2>
              <p>
                Top 50 Club Reward Hodl and DCA your way to be a Top 50 club and
                claim daily rewards incentivized specifically for them.
              </p>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="front">
            <img
              src={top4}
              alt="top4"
              className="object-cover sm:w-[200px] sm:h-[170px] lg:w-[170px] lg:h-[270px] 
              md:w-[270px] md:h-[230px] md:object-cover lg:object-cover"
            />
          </div>
          <div class="back">
            <div class="back-content middle">
              <h2>Upcoming NFTs</h2>
              <p>
                Experience fun and simple minigame concept and unlimited
                possibilities for lifetime earnings.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FeaturesList;
