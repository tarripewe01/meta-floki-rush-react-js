import React from 'react';

const Token = () => {
  return <div className="w-screen bg-slate-600 flex justify-center items-center px-3 gap-2">
  <div className="bg-red-400 justify-center items-center">
    <img src={token1} alt="token1" className="w-[150px]" />
  </div>
  <div className="bg-orange-300 w-screen space-y-2">
    <p className="text-sm text-[#3AB682] font-bold">Total Supply</p>
    <p className="text-xs">1,000,000,000,000</p>
  </div>
</div>;
};

export default Token;
