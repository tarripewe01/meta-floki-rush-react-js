import React from "react";
import bgInfo from "../../../assets/image/bgInfo.png";
import "animate.css/animate.min.css";

const Info = () => {
  return (
    <div
      id="Home"
      className="flex flex-col items-center justify-center top-[220px] 
      md:top-[350px] -translate-y-[24rem] lg:-translate-y-[22rem] 
      xl:-translate-y-[23rem] 
     2xl:-translate-y-[25rem]"
    >
      <img
        src={bgInfo}
        alt="bgInfo"
        className="w-4/5 lg:w-[50rem]  xl:w-[60rem] 2xl:w-[70rem]"
      />
      <div
        className="absolute w-[450px] sm:w-[250px] xl:w-[600px] 2xl:w-[1000px] justify-center text-center text-white font-[Inter] 
    -translate-y-[1rem] sm:-translate-y-[0.5rem] xl:-translate-y-[2rem] 
    2xl:-translate-y-3rem 2xl:px-20"
      >
        <p
          className="font-bold text-[40px] mb-4 sm:mb-2 lg:text-[50px] 
          xl:text-[60px] 
        2xl:text-[70px] sm:text-[15px] "
        >
          FlokiRush
        </p>
        <p
          className=" text-[15px] mb-10 sm:mb-4 sm:text-[8px] 
        lg:text-[20px] xl:text-[30px] 2xl:text-[30px] 2xl:mb-[50px]  "
        >
          FlokiRush invites you to Rush along exciting journey to have tons of
          fun and make lots of money!!
        </p>
        <p
          className=" text-neutral-800 font-semibold sm:text-[8px] 
        lg:text-[23px] xl:text-[28px] 2xl:text-[30px] 2xl:pt-[50px]"
        >
          Get Ready To Rush Your Way To Moon
        </p>
      </div>
    </div>
  );
};

export default Info;
