import React from "react";
import buy1 from "../../../assets/svg/buy1.svg";
import buy2 from "../../../assets/svg/buy2.svg";
import buy3 from "../../../assets/svg/buy3.svg";
import buy4 from "../../../assets/svg/buy4.svg";
import buy5 from "../../../assets/svg/buy5.svg";

const HowToBuyList = () => {
  return (
    <div className="flex items-center justify-center font-[Inter] text-[#862D2D]">
      <div   className="grid gap-2 lg:gap-4 xl:gap-8 2xl:gap-12">
        <div
          className="grid grid-cols-3 sm:grid-cols-1 mx-auto items-center 
        justify-center gap-2 lg:gap-6 xl:gap-8 2xl:gap-12"
        >
          <div
            className="bg-[#EBEBEB] h-[230px] border-8 border-[#FFF568] w-3/5 rounded-md mx-auto 
        py-2 md:w-[230px] lg:w-[260px] lg:h-[300px] xl:w-[330px] xl:h-[350px] xl:border-[15px]
        2xl:w-[330px] 2xl:h-[350px] 2xl:border-[15px]"
          >
            <div
              className="flex w-full items-center justify-center place-items-center mx-auto 
            justify-self-center justify-items-center"
            >
              <img
                src={buy1}
                alt="buy1"
                className="w-[90px] h-[90px] lg:w-[120px] lg:h-[120px] xl:w-[120px] xl:h-[120px] 
                2xl:w-[120px] 2xl:h-[120px]"
              />
            </div>
            <div className="px-2 lg:px-3 xl:px-5 2xl:px-3">
              <p className="text-sm font-bold lg:text-lg xl:text-xl 2xl:text-xl ">
                CREATE WALLET
              </p>
              <p className="text-xs leading-relaxed mt-3 lg:text-base xl:text-lg 2xl:text-lg">
                Download TrustWallet or Metamask, the apps are secure and widely
                used in the DeFi Market
              </p>
            </div>
          </div>
          <div
            className="bg-[#EBEBEB] h-[230px] border-8 border-[#4ABDB7] w-3/5 rounded-md mx-auto 
        py-2 md:w-[230px] lg:w-[260px] lg:h-[300px] xl:w-[330px] xl:h-[350px] xl:border-[15px] 
        2xl:w-[330px] 2xl:h-[350px] 2xl:border-[15px]"
          >
            <div
              className="flex w-full items-center justify-center place-items-center mx-auto 
            justify-self-center justify-items-center"
            >
              <img
                src={buy2}
                alt="buy2"
                className="w-[90px] h-[90px] lg:w-[120px] lg:h-[120px] xl:w-[120px] xl:h-[120px] 
                2xl:w-[120px] 2xl:h-[120px]"
              />
            </div>
            <div className="px-2 lg:px-3 xl:px-5 2xl:px-3">
              <p className="text-sm font-bold lg:text-lg xl:text-xl 2xl:text-xl ">
                FUND YOUR WALLET
              </p>
              <p className="text-xs leading-relaxed mt-3 lg:text-base xl:text-xl 2xl:text-lg">
                Fund your wallet by purchasing BNB. Send BNB to your Metamask
                Account
              </p>
            </div>
          </div>
          <div
            className="bg-[#EBEBEB] h-[230px] border-8 border-[#3CB878] w-3/5 rounded-md mx-auto 
        py-2 md:w-[230px] lg:w-[260px] lg:h-[300px] xl:w-[330px] xl:h-[350px] xl:border-[15px] 
        2xl:w-[330px] 2xl:h-[350px] 2xl:border-[15px] "
          >
            <div
              className="flex w-full items-center justify-center place-items-center mx-auto 
            justify-self-center justify-items-center"
            >
              <img
                src={buy3}
                alt="buy3"
                className="w-[90px] h-[90px] lg:w-[120px] lg:h-[120px] xl:w-[120px] xl:h-[120px] 
                2xl:w-[120px] 2xl:h-[120px]"
              />
            </div>
            <div className="px-2 lg:px-3 xl:px-5 2xl:px-3">
              <p className="text-sm font-bold lg:text-lg xl:text-xl 2xl:text-xl ">
                GO TO BROWSER
              </p>
              <p className="text-xs leading-relaxed mt-3 lg:text-base xl:text-lg 2xl:text-lg">
                Use your Chrome and Visit Pancakeswap.finance
              </p>
            </div>
          </div>
        </div>
        <div className="grid grid-cols-2 sm:grid-cols-1 mx-auto items-center justify-center gap-2 lg:gap-6 xl:gap-8 2xl:gap-12 ">
          <div
            className="bg-[#EBEBEB] h-[230px] border-8 border-[#C69C6D] w-3/5 rounded-md mx-auto 
            py-2 md:w-[230px] lg:w-[260px] lg:h-[300px] xl:w-[330px] xl:h-[350px] xl:border-[15px] 
            2xl:w-[330px] 2xl:h-[350px] 2xl:border-[15px]"
          >
            <div
              className="flex w-full items-center justify-center place-items-center mx-auto 
            justify-self-center justify-items-center"
            >
              <img
                src={buy4}
                alt="buy4"
                className="w-[90px] h-[90px] lg:w-[120px] lg:h-[120px] xl:w-[130px] xl:h-[130px]
                2xl:w-[120px] 2xl:h-[120px]"
              />
            </div>
            <div className="px-2 lg:px-3 2xl:px-3">
              <p className="text-sm font-bold lg:text-lg xl:text-xl 2xl:text-xl ">
                INSERT <br /> METAFLOKIRUSH CA
              </p>
              <p className="text-xs leading-relaxed mt-3 lg:text-base xl:text-lg 2xl:text-lg">
                Click “Select a Currency” and enter MetaFlokiRush contract
                address
              </p>
            </div>
          </div>
          <div
            className="bg-[#EBEBEB] h-[230px] border-8 border-[#F49AC1] w-3/5 rounded-md mx-auto 
            py-2 md:w-[230px] lg:w-[260px] lg:h-[300px] xl:w-[330px] xl:h-[350px] xl:border-[15px] 
            2xl:w-[330px] 2xl:h-[350px] 2xl:border-[15px]"
          >
            <div
              className="flex w-full items-center justify-center place-items-center mx-auto 
            justify-self-center justify-items-center"
            >
              <img
                src={buy5}
                alt="buy5"
                className="w-[90px] h-[90px] lg:w-[120px] lg:h-[120px] xl:w-[120px] xl:h-[120px]
                2xl:w-[120px] 2xl:h-[120px]"
              />
            </div>
            <div className="px-2 lg:px-3 2xl:px-3">
              <p className="text-sm font-bold lg:text-lg xl:text-xl 2xl:text-xl ">
                PURCHASE $FLUSH
              </p>
              <p className="text-xs leading-relaxed mt-3 lg:text-base xl:text-lg 2xl:text-lg">
                use the slippage 12% to 15% and purchase $FLUSH through
                PancakeSwap
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HowToBuyList;
