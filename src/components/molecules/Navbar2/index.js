import React from "react";
import Button from "../../atoms/Button";
import Logo from "../../atoms/Logo";
import Medsos from "../../atoms/Medsos";
import Navlink from "../../atoms/Navlink";
import NavbarMobile from "../NavbarMobile";


const Navbar2 = () => {
  
  return (
    <div className="flex items-center justify-center ">
      <NavbarMobile />
      <Button />
      {/* <Ribbon /> */}
      <Navlink />
      <Medsos />
      <Logo />
    </div>
  );
};

export default Navbar2;
