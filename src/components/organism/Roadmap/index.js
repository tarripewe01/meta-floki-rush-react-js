import React from "react";
import RoadmapList from "../../molecules/RoadmapList";
import bgBrown from "../../../assets/image/bgBrown.png";
import flokiFun from "../../../assets/image/flokiFun.png";
import ribbonRoadmap from "../../../assets/image/ribbonRoadmap.png";

const Roadmap = () => {
  return (
    <div
      id="Roadmap"
      style={{ backgroundImage: `url(${bgBrown})` }}
      className="w-full items-center justify-center -translate-y-[420px] 
      md:-translate-y-[100px] lg:-translate-y-[330px] xl:-translate-y-[330px] 
      2xl:-translate-y-[1300px] "
    >
      <div  className="flex items-center justify-center translate-y-10 ">
        <img src={ribbonRoadmap} alt="ribbonRoadmap" className="md:h-[150px]"  />
      </div>
      <div
        className="grid grid-cols-1 mx-auto font-[Inter] text-black pb-10 lg:grid-cols-2 
      lg:-ml-10 xl:grid-cols-2 2xl:grid-cols-2 justify-center items-center"
      >
        <div
          className="md:hidden lg:ml-[90px] xl:ml-[30px] 2xl:ml-[170px] sm:ml-[50px] 
        justify-center items-center "
        >
          <img
            src={flokiFun}
            alt="flokiFun"
            className="w-[200px] lg:w-[350px] xl:w-[450px] 2xl:w-[550px] "
          />
        </div>
        <RoadmapList />
      </div>
    </div>
  );
};

export default Roadmap;
