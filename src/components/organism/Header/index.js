import React from "react";
import bgVideo from "../../../assets/video/bgVideo.mp4";
import Navbar2 from "../../molecules/Navbar2";

const Header = () => {
  return (
    <div>
      <video autoPlay loop muted className="-z-50 bg-cover w-full">
        <source src={bgVideo} type="video/mp4" />
      </video>
      <Navbar2 />
    </div>
  );
};

export default Header;
