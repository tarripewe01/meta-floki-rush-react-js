import React from "react";
import { HowToBuyList } from "../..";
import bgBuy from "../../../assets/image/bgBuy.jpg";
import imgBuy from "../../../assets/image/imgBuy.png";

const HowToBuy = () => {
  return (
    <div
      style={{ backgroundImage: `url(${bgBuy})` }}
      className="bg-no-repeat bg-cover -translate-y-[650px] h-[1500px] md:h-[850px] lg:h-[1000px] xl:h-[1200px] 2xl:h-[1200px]
      text-center items-center justify-center md:-translate-y-[100px] lg:-translate-y-[350px] 
      xl:-translate-y-[350px] 2xl:-translate-y-[1350px]"
    >
      <div className="flex items-center justify-center">
        <img
          src={imgBuy}
          alt="imgBuy"
          className="xl:w-[850px] 2xl:w-[900px] 2xl:-translate-y-20"
        />
      </div>
      <div className="md:-mt-[50px] xl:-mt-[90px] 2xl:-mt-[200px]">
        <HowToBuyList />
      </div>
    </div>
  );
};

export default HowToBuy;
