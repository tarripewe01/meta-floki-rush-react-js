import React from "react";
import { Info, Support } from "../..";
import bgBlue from "../../../assets/image/bgBlue.png";
import FeatureList2 from "../../molecules/FeatureList2";
import Info2 from "../../molecules/Info2";

const Features = () => {
  return (
    <div
      style={{ backgroundImage: `url(${bgBlue})` }}
      className="flex flex-col justify-center items-center bg-no-repeat 
      h-[100rem] xl:h-[120rem] 2xl:h-[180rem] sm:h-[150rem] bg-cover mx-10 
      sm:mx-0 -mt-5 lg:mx-24 xl:mx-24 2xl:mx-28"
    >
      <div
        className="translate-y-[270px] md:translate-y-[220px] 
        lg:translate-y-[100px] xl:translate-y-[50px] 
      2xl:-translate-y-[370px] sm:translate-y-[100px]"
      >
        <Info />
        <Support />
        <Info2 />
        {/* <FeaturesList /> */}
        <FeatureList2 />
      </div>
    </div>
  );
};

export default Features;
