import Header from "./Header";
import Features from "./Features";
import Roadmap from "./Roadmap";
import Tokenomics from "./Tokenomics";
import HowToBuy from "./HowToBuy";
import Footer from "./Footer";

export { Header, Features, Roadmap, Tokenomics, HowToBuy, Footer };
