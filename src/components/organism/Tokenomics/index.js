import React from "react";
import { TokenList } from "../..";
import bgSky from "../../../assets/image/bgSky.png";

const Tokenomics = () => {
  return (
    <div
    
      className="grid w-full h-[1000px] md:h-[400px] lg:h-[600px] xl:h-[700px] 2xl:h-[800px] bg-no-repeat bg-cover 
      -translate-y-[450px] md:-translate-y-[100px] items-center 
      justify-center text-center lg:-translate-y-[350px] xl:-translate-y-[350px] 
      2xl:-translate-y-[1320px] "
      style={{ backgroundImage: `url(${bgSky})` }}
    >
      <h1
      // data-aos="fade-right"
        className="text-[#922626] text-2xl font-bold font-[Inter] sm:-mt-[70px] 
      md:text-4xl md:mt-5 lg:text-6xl lg:mt-6 xl:text-7xl xl:mt-8 2xl:text-6xl"
      >
        TOKENOMICS
      </h1>

      <div
        className="-translate-y-[150px] md:translate-y-[50px] 
      lg:translate-y-[50px] xl:-translate-y-[20px] 2xl:-translate-y-[30px]"
      >
        <TokenList />
      </div>
    </div>
  );
};

export default Tokenomics;
