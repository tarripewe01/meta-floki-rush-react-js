import React from "react";
import bgFooter from "../../../assets/image/bgFooter.png";
import instagram from "../../../assets/svg/instagram.svg";
import reddit from "../../../assets/svg/reddit.svg";
import telegram from "../../../assets/svg/telegram.svg";
import twitter from "../../../assets/svg/twitter.svg";

const Footer = () => {
  return (
    <div
      style={{ backgroundImage: `url(${bgFooter})` }}
      className="flex w-full bg-no-repeat bg-cover mx-auto 
      -translate-y-[700px] h-[390px] pt-[100px] text-center items-center 
      justify-center pb-[90px] md:-translate-y-[130px] 
      lg:-translate-y-[400px] md:h-[350px] lg:h-[300px] 
      lg:pt-[150px] xl:-translate-y-[400px] 2xl:pt-[200px] 
      2xl:-translate-y-[1500px] 2xl:h-[350px] -mb-[3000px]
     "
    >
      <div className="font-[Inter] text-[#8C3939] 2xl:px-[200px] xl:-ml-20">
        <div
          className=" grid-cols-4 grid sm:grid-cols-1 md:grid-cols-2 mx-auto px-6 
          text-center items-center justify-center gap-3 lg:items-start lg:justify-start 
          xl:items-start xl:justify-start 2xl:items-start 2xl:justify-start xl:ml-20 
          xl:pt-16 2xl:ml-28 "
        >
          <div>
            <p
              className="font-bold md:text-lg lg:text-left lg:text-xl xl:text-3xl xl:text-left 
            2xl:text-left 2xl:text-2xl "
            >
              CURIOUS? <br />
              GET IN TOUCH
            </p>
            <p
              className="text-xs mt-3 md:text-base lg:text-left lg:text-md xl:text-lg xl:text-left 
            2xl:text-left 2xl:text-lg "
            >
              We can’t promise a reply, but we’d love to hear your thoughts
            </p>
          </div>
          <div>
            <p
              className="font-bold md:text-lg lg:text-left lg:text-2xl xl:text-3xl xl:text-left 
            2xl:text-left 2xl:text-2xl"
            >
              INFO
            </p>
            <p
              className="text-xs mt-3 md:text-base lg:text-left lg:text-lg xl:text-lg xl:text-left 
            2xl:text-left 2xl:text-lg"
            >
              Term and Conditions
            </p>
          </div>
          <div>
            <p
              className="font-bold md:text-lg lg:text-left lg:text-2xl xl:text-3xl xl:text-left 
            2xl:text-left 2xl:text-2xl"
            >
              FOLLOW US
            </p>
            <div
              className="flex sm:gap-1 md:gap-1 items-center justify-center mt-3 lg:justify-start 
            xl:justify-start 2xl:text-left 2xl:justify-start"
            >
              <a href="https://t.me/metaflokirush" target="_blank">
                <img
                  src={telegram}
                  alt="telegram"
                  className="w-[25px] h-[25px] lg:w-[40px] lg:h-[40px] 
                  xl:w-[47px] xl:h-[47px] 2xl:w-[40px] 2xl:h-[40px] hover:animate-spin-slow"
                />
              </a>
              <a href="https://twitter.com/metaflokirush" target="_blank">
                <img
                  src={twitter}
                  alt="twitter"
                  className="w-[25px] h-[25px] md:w-[24px] md:h-[24px] md:ml-[4px]   
                  lg:w-[40px] lg:h-[40px] lg:ml-[4px] 
                  xl:w-[47px] xl:h-[47px] xl:ml-[7px] 2xl:w-[39px] 
                  2xl:h-[39px] 2xl:ml-[8px] hover:animate-spin-slow"
                />
              </a>
              <a
                href="https://www.instagram.com/meta.flokirush/"
                target="_blank"
              >
                <img
                  src={instagram}
                  alt="instagram"
                  className="w-[30px] h-[30px]  lg:w-[50px] lg:h-[50px] 
                  xl:w-[60px] xl:h-[60px] 2xl:w-[49px] 2xl:h-[49px] hover:animate-spin-slow"
                />
              </a>
              <a
                href="https://www.reddit.com/user/metaflokirush"
                target="_blank"
              >
                <img
                  src={reddit}
                  alt="reddit"
                  className="w-[26px] h-[26px] md:w-[26px] md:h-[26px] lg:w-[42px] lg:h-[42px] 
                  xl:w-[52px] xl:h-[52px] 2xl:w-[40px] 2xl:h-[40px] hover:animate-spin-slow"
                />
              </a>
            </div>
          </div>
          <div>
            <p
              className="font-bold md:text-lg lg:text-left lg:text-2xl xl:text-3xl xl:text-left 
            2xl:text-left 2xl:text-2xl"
            >
              CONTACT US
            </p>

            <a
              href="mailto: metaflokirush@gmail.com"
              className="text-xs mt-3 md:text-base lg:text-left lg:text-lg xl:text-lg xl:text-left xl:-ml-[50px] 
            2xl:text-left 2xl:text-lg 2xl:-ml-[80px]  "
            >
              metaflokirush@gmail.com
            </a>
          </div>
        </div>
        <div
          className="mt-[45px] items-end justify-end text-end lg:mt-[90px] 
        xl:mt-[140px] 2xl:mt-[30px]"
        >
          <p
            className="text-[10px] md:mt-[70px] md:text-[13px] lg:text-xl  xl:text-lg 
          2xl:text-lg "
          >
            Copyright © 2022 MetaFlokiRush. All Right Reserved.
          </p>
        </div>
      </div>
    </div>
  );
};

export default Footer;
