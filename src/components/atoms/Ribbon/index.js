import React from "react";
import ribbonYellow from "../../../assets/image/ribbonYellow.png";
import { Link } from "react-router-dom";

const Ribbon = () => {
  return (
    <div
      className="flex sm:hidden w-full items-center justify-center top-0 absolute pt-8 
  md:w-2/3 lg:w-[600px] xl:w-3/4 2xl:w-[5500px] "
    >
      <img src={ribbonYellow} alt="ribbonYellow"  />
      {/* <div>
      <a href="#Home" className="hover:text-[#7a2605]">
        Home
      </a>
      <a href="#TopFeatures" className="hover:text-[#7a2605]">
        Features
      </a>
      <a href="#Roadmap" className="hover:text-[#7a2605]">
        Roadmap
      </a>
      <a className="hover:text-[#7a2605]">Whitepaper</a>

      <Link to="/minigames" target="_blank" className="hover:text-[#7a2605]">
        Minigame
      </Link>

      <Link to="/marketplace" target="_blank" className="hover:text-[#7a2605]">
        Marketplace
      </Link>
      </div> */}
    </div>
  );
};

export default Ribbon;
