/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { Link } from "react-router-dom";

const Navlink = () => {
  return (
    <div
      // style={{textShadow: '2px 3px #FFF'}}
      className="flex sm:hidden absolute font-[Poppins] text-sm 
      text-[#7a2605] space-x-1 uppercase
      top-16 md:text-[9px] md:space-x-3 lg:text-[13px] lg:top-20 
      lg:space-x-2 xl:text-[15px] xl:top-[80px] 
      xl:space-x-[0.40rem] 2xl:text-[20px] 2xl:top-[85px]
      2xl:space-x-[0.30rem] cursor-pointer z-50 lg:-translate-y-4 gap-5 lg:gap-3 md:gap-1
      "
    >
      <a
        href="#Home"
        className=" bg-[#FFC84E] px-3 py-2 
      rounded-lg border-2 border-[#7a2605] hover:bg-[#7a2605] 
      hover:text-[#FFC84E] w-[200px] text-center xl:w-[150px] lg:w-[130px] md:w-[90px] "
      >
        Home
      </a>
      <a
        href="#TopFeatures"
        className=" bg-[#FFC84E] px-3 py-2 
      rounded-lg border-2 border-[#7a2605] hover:bg-[#7a2605] 
      hover:text-[#FFC84E] w-[200px] text-center xl:w-[150px] lg:w-[130px] md:w-[90px] "
      >
        Features
      </a>
      <a
        href="#Roadmap"
        className=" bg-[#FFC84E] px-3 py-2 
      rounded-lg border-2 border-[#7a2605] hover:bg-[#7a2605] 
      hover:text-[#FFC84E] w-[200px] text-center xl:w-[150px] lg:w-[130px] md:w-[90px] "
      >
        Roadmap
      </a>
      <a
        href=""
        className=" bg-[#FFC84E] px-3 py-2 
      rounded-lg border-2 border-[#7a2605] hover:bg-[#7a2605] 
      hover:text-[#FFC84E] w-[200px] text-center xl:w-[150px] lg:w-[130px] md:w-[90px] "
      >
        Whitepaper
      </a>
      <Link
        to="/minigames"
        target="_blank"
        className="bg-[#FFC84E] px-3 py-2 
        rounded-lg border-2 border-[#7a2605] hover:bg-[#7a2605] 
        hover:text-[#FFC84E] w-[200px] text-center xl:w-[150px] lg:w-[130px] md:w-[90px] "
      >
        Minigame
      </Link>

      <Link
        to="/marketplace"
        target="_blank"
        className="bg-[#FFC84E] px-3 py-2 
      rounded-lg border-2 border-[#7a2605] hover:bg-[#7a2605] 
      hover:text-[#FFC84E] w-[200px] text-center xl:w-[150px] lg:w-[130px] md:w-[90px] "
      >
        Marketplace
      </Link>
    </div>
  );
};

export default Navlink;
