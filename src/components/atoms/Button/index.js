import React, { useState } from "react";
import dogeBank from "../../../assets/svg/dogeBank.svg";

const Button = () => {
  return (
    <div
      className=" flex sm:hidden w-full z-50 absolute top-0 items-center
    justify-end pr-10 pt-5 2xl:translate-y-[400px] 
    xl:translate-y-[300px]
    lg:translate-y-[250px]
    md:translate-y-[200px]"
    >
      <a href="/" className="z-50 cursor-pointer">
        <img
          src={dogeBank}
          alt="dogeBank"
          className="hover:animate-spin-slow w-[100px] h-[100px] 
          md:w-[50px] md:h-[50px] 
          lg:w-[60px] lg:h-[60px]  
          xl:w-[90px] xl:h-[90px]  "
        />
      </a>
    </div>
  );
};

export default Button;
