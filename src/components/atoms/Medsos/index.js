import React from "react";
import instagram from "../../../assets/svg/instagram.svg";
import reddit from "../../../assets/svg/reddit.svg";
import telegram from "../../../assets/svg/telegram.svg";
import twitter from "../../../assets/svg/twitter.svg";

const Medsos = () => {
  return (
    <div className="flex sm:hidden absolute top-40 w-full pl-10 lg:top-48 xl:top-48 2xl:top-64 z-30 ">
      <div
        className="flex flex-col bg-[#FFC84E] w-[50px] h-[200px] rounded-3xl justify-evenly items-center 
    lg:h-[270px] lg:w-[60px] xl:h-[370px] xl:w-[80px] xl:rounded-full 2xl:w-[90px] 2xl:h-[420px] 2xl:rounded-full "
      >
        <a href="https://t.me/metaflokirush" target="_blank" rel="noreferrer">
          <img
            src={telegram}
            alt="telegram"
            className="h-[30px] w-[30px] lg:h-[40px] lg:w-[40px] 
            xl:w-[50px] xl:h-[50px] 2xl:w-[60px] 2xl:h-[60px] hover:animate-spin-slow"
          />
        </a>
        <a href="https://twitter.com/metaflokirush" target="_blank">
          <img
            src={twitter}
            alt="twitter"
            className="h-[30px] w-[30px] lg:h-[40px] lg:w-[40px] 
            xl:w-[50px] xl:h-[50px] 2xl:w-[60px] 2xl:h-[60px] hover:animate-spin-slow"
          />
        </a>
        <a href="https://www.instagram.com/meta.flokirush/" target="_blank">
          <img
            src={instagram}
            alt="instagram"
            className="h-[35px] w-[35px] lg:h-[47px] lg:w-[47px] 
            xl:w-[58px] xl:h-[58px] 2xl:w-[70px] 2xl:h-[70px] hover:animate-spin-slow"
          />
        </a>
        <a href="https://www.reddit.com/user/metaflokirush" target="_blank">
          <img
            src={reddit}
            alt="reddit"
            className="h-[30px] w-[30px] lg:h-[40px] lg:w-[40px] 
            xl:w-[50px] xl:h-[50px] 2xl:w-[60px] 2xl:h-[60px] hover:animate-spin-slow"
          />
        </a>
      </div>
    </div>
  );
};

export default Medsos;
