import React from "react";
import logo2 from "../../../assets/image/logo2.png";

const Logo = () => {
  return (
    <div
      className="flex absolute top-[150px] w-full justify-center items-center 
      sm:top-[70px]
      lg:top-[170px]
      xl:top-[150px] 2xl:top-[300px]"
    >
      <div className=" w-[120px] md:w-[300px] lg:w-[350px] xl:w-2/4 2xl:w-[600px] ">
        <img
          src={logo2}
          alt="logo2"
          className="hover:animate-bounce cursor-pointer"
        />
      </div>
    </div>
  );
};

export default Logo;
