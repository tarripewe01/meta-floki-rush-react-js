import Home from "./page/Home";
import Minigame from "./page/Minigame";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import AOS from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";
import Marketplace from "./page/Marketplace";

function App() {
  useEffect(() => {
    AOS.init({
      // delay: 200,
      duration: 500,
    });
  }, []);

  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/minigames" element={<Minigame />} />
          <Route path="/marketplace" element={<Marketplace />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
