import React from "react";
import {
  Features,
  Footer,
  Header,
  HowToBuy,
  Roadmap,
  Tokenomics,
} from "../../components";

const Home = () => {
  return (
    <>
      <Header />
      <Features />
      <Roadmap />
      <Tokenomics />
      <HowToBuy />
      <Footer />
    </>
  );
};

export default Home;
