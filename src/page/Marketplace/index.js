import React from "react";
import logocomingsoon from "../../assets/image/logoComingSoon.png";
import bg from "../../assets/svg/bgComingSoon.svg";

const Marketplace = () => {
  return (
    <div
      style={{ backgroundImage: `url(${bg})` }}
      className="bg-cover bg-no-repeat"
    >
      <img
        src={logocomingsoon}
        alt="logocomingsoon"
        className="w-[2000px] h-screen "
      />
    </div>
  );
};

export default Marketplace;
